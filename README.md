# Abyss

一个可以匿名，使用标签管理串的讨论版：[Abyss](https://abyss.club)。
致力于提供一个高效、方便使用的次文化主题的讨论环境。语言环境为中文。

* 后端代码仓库：[uexky](https://gitlab.com/abyss.club/uexky)
* 前端代码仓库：[tt392](https://gitlab.com/abyss.club/tt392)

## 目标

此项目的初衷和目的：

1. 高效的讨论环境：话题的聚合和分离，完善的通知
1. 方便使用：提供简洁、现代化的界面和交互体验

设计和开发以上述两点出发，并以此为目标优化。此外附加一条原则：

* 尊重隐私：不收集用户身份信息，尽量不收集不必要的其他数据

## 开发需求及相关名词

以上述三点目标和原则出发的主要需求：

1. 以 `标签/Tag` 来标注 `讨论串/串/Thread`，而不是通过多级分区来隔离。标签只有两级，
分为 `主标签/MainTag` 和 `子标签/SubTag`。主标签仅作大范围的话题区分，每个帖子有且仅有一个。
而子标签最多可以有四个
1. `账户/Account` 可以订阅标签，登陆用户打开首页即为已订阅标签的内容的聚合
订阅不区分主标签和子标签。（TODO：可以拥有多个订阅组）
1. （TODO：每个 tag 可以有自己的描述和专页（或时置顶贴），用以说明自身）
1. （TODO：用户可以对标签的合并、提升作出反馈，满足一定条件之后处理）
1. 网站设计与开发需要兼顾在手机上和电脑上的使用（TODO：支持 PWA），并提供良好的文字排版
1. 尽量减少页面重新加载，使用时给与足够的过渡与反馈
1. 串，`帖/Post`，被 `回复/Reply` 和 `引用/Refer` 时需要在界面上通知用户（TODO：支持浏览器通知）
1. 发表串或贴时。可以选择匿名，或者从最多三个 `用户/用户名/name` 当中选择一个身份。
其他人无法获知不同用户之间的关联性
1. 匿名发表时，将会生成 `匿名ID/Anonymous ID` 作为发言的用户名。
同一个账户下，同一个串内的匿名 ID 相同、不同串内不同
1. `登录/注册/Sign In` 流程一致，除了邮箱以外不需要提供任何信息，包括密码

由此衍生出的需求：

### 串与贴

1. 主标签只能从现有主标签中选择，子标签可以自行添加
1. 一个串可以没有标题
1. 使用 Markdown 格式发帖，支持链接、图片等内容。不允许添加 HTML 代码
1. （TODO：帖子内容可以编辑并可查看编辑历史）
1. （可能的需求：支持主流视频网站的内嵌播放器）
1. 回复串时，可以对串中的贴进行引用，最多可以引用三贴
1. 点击被引用的回复可以滚动到被回复的帖子，并提供一个按钮返回，按下之后滚动到原处
1. （可能的需求：会话形式查看多贴间的相互回复）

### 管理员

* 封锁、屏蔽串或贴
* 封禁用户、账户或IP
* (TODO) 修改帖子 tag
* (TODO) 删除，合并，提升 tag
* (TODO) 在帖子中附上管理员批注
* 解除上述的操作

## API

API 使用 [graphql](https://graphql.org/) 的方式 , schema 见此：[schema](https://gitlab.com/abyss.club/abyss/blob/master/api.gql)。